const express = require('express');
const { Pool } = require('pg');

const app = express();
const PORT = process.env.PORT;
const pool = new Pool({
    user: 'moj_user',
    host: 'localhost',
    database: 'moja_baza',
    password: 'moje_haslo',
    port: 5432,
});

app.get('/exact', async (req, res) => {
    try {
        const searchParam = req.query.search;
        const query = `SELECT * FROM parking_tickets WHERE plate = $1`;
        const result = await pool.query(query, [searchParam]);
        res.json(result.rows);
    } catch (error) {
        console.error('Błąd przy zapytaniu do bazy danych:', error);
        res.status(500).send('Wystąpił błąd przy przetwarzaniu zapytania.');
    }
});

app.get('/like', async (req, res) => {
    try {
        const searchParam = req.query.search;
        const query = `SELECT * FROM parking_tickets WHERE plate LIKE '%' || $1 || '%'`;
        const result = await pool.query(query, [searchParam]);
        res.json(result.rows);
    } catch (error) {
        console.error('Błąd przy zapytaniu do bazy danych:', error);
        res.status(500).send('Wystąpił błąd przy przetwarzaniu zapytania.');
    }
});

app.get('/test', async (req, res) => {
    const result = await pool.query(`SELECT * FROM parking_tickets WHERE plate = 'ABC'`)

    await pool.query(`UPDATE parking_tickets SET num = $1 + 1 WHERE plate = 'ABC'`, [result.rows[0].num])
    res.json()
})

app.listen(PORT, () => {
    console.log(`Serwer działa na porcie ${PORT}`);
});
